package com.lez22.ProgettoChatSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgettoChatSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgettoChatSpringApplication.class, args);
	}

}
