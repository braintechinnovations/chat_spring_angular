package com.lez22.ProgettoChatSpring.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="messaggi")
public class Messaggio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="messaggioID")
	private int id;
	
	@Column
	private String username;
	@Column
	private String contenuto;
	
	@Column(insertable=false)
	private String data_messaggio;
	
	public Messaggio() {
		
	}
	
	public Messaggio(int id, String username, String contenuto, String data_messaggio) {
		super();
		this.id = id;
		this.username = username;
		this.contenuto = contenuto;
		this.data_messaggio = data_messaggio;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getContenuto() {
		return contenuto;
	}
	public void setContenuto(String contenuto) {
		this.contenuto = contenuto;
	}
	public String getData_messaggio() {
		return data_messaggio;
	}
	public void setData_messaggio(String data_messaggio) {
		this.data_messaggio = data_messaggio;
	}
	
	
	
}
