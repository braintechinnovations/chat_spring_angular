package com.lez22.ProgettoChatSpring.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lez22.ProgettoChatSpring.models.Messaggio;
import com.lez22.ProgettoChatSpring.repositories.DataAccessRepo;

@Service
public class MessaggioService implements DataAccessRepo<Messaggio> {

	@Autowired
	private EntityManager ent_man;
	
	private Session getSessione() {
		return ent_man.unwrap(Session.class);
	}
	
	@Override
	public Messaggio insert(Messaggio t) {

		Messaggio temp = new Messaggio();
		temp.setUsername(t.getUsername());
		temp.setContenuto(t.getContenuto());
		
		Session sessione = this.getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return null;
	}

	@Override
	public List<Messaggio> findAll() {
		Session sessione = getSessione();
		return sessione.createCriteria(Messaggio.class).list();
	}

}
