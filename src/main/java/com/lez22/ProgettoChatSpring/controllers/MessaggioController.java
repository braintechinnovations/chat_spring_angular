package com.lez22.ProgettoChatSpring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez22.ProgettoChatSpring.models.Messaggio;
//import com.lez22.ProgettoChatSpring.models.StatusResponse;
import com.lez22.ProgettoChatSpring.services.MessaggioService;

class StatusResponse{
	private String status;
	private Messaggio mess;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Messaggio getMess() {
		return mess;
	}
	public void setMess(Messaggio mess) {
		this.mess = mess;
	}
}

@RestController
@RequestMapping("/messaggio")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MessaggioController {
 
	@Autowired
	private MessaggioService service;
	
	@PostMapping("/insert")
	public StatusResponse inserisciMessaggio(@RequestBody Messaggio objMessaggio) {
		StatusResponse resp = new StatusResponse();
		Messaggio temp = this.service.insert(objMessaggio);
		
		if(temp != null && temp.getId() > 0) {
			resp.setStatus("SUCCESS");
			resp.setMess(temp);
		}
		else {
			resp.setStatus("ERROR");
			resp.setMess(null);
		}
		
		return resp;
	}
	
	@GetMapping("/list")
	public List<Messaggio> listaMessaggi(){
		return this.service.findAll();
	}
	
}
