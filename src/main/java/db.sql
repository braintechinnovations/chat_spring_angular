DROP DATABASE IF EXISTS progetto_chat;
CREATE DATABASE progetto_chat;
USE progetto_chat;

CREATE TABLE messaggi(
	messaggioID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(250),
    contenuto TEXT NOT NULL,
    data_messaggio DATETIME DEFAULT NOW()
);

INSERT INTO messaggi(username, contenuto) VALUE
("CICCIO", "Ciao sono Ciccio");

SELECT * FROM messaggi;